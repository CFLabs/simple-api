# Prevent attemts to strip binaries as this is what we want to push.
%global __os_install_post %{nil}

Name:                   simpleAPI
#Version:               0.0.1
Version:				%{?VERSION_NUMBER}
Release:                %{?BUILD_NUMBER}
Summary:                Simple API
Packager:               Richard Nason <rnason@awsdocs.com>
License:                AWSDOCS
Group:                  System/Base
Vendor:                 AWSDOCS
Url:                    http://awsdocs.com
#Source:                %{name}.tar.gz
BuildArch:              x86_64
BuildRoot:              %{_tmppath}/%{name}-%{version}-tmp
#BuildRequires:
# Requires:             
# Requires: dialog if attemting to use the TUI

%description
"SimpleAPI is simply an python based bottle API server that can be used for testing purposes of various CI/CD jobs"

%prep
# There is nothing to prep as we are dealing with pre-built binaries.
#%setup -n %{name}

%build
# This section will remain empty as we are not building anything other than the RPM

%install

# create directories where the files will be located
mkdir -p $RPM_BUILD_ROOT/usr/local/bin/
mkdir -p $RPM_BUILD_ROOT/etc/systemd/system

# Deploy the binary files (-m argument = permissions)
install -m 554 $RPM_SOURCE_DIR/simpleAPI $RPM_BUILD_ROOT/usr/local/bin/simpleAPI
install -m 554 $RPM_SOURCE_DIR/simpleAPI.service $RPM_BUILD_ROOT/etc/systemd/system/simpleAPI.service

%pre

# Pre install commands or scripts if any
API=`systemctl list-units | grep simpleAPI.service`

# Check and stop simpleAPI.service
if [ -z "$API" ]; then
	echo -e "existing simpleAPI.service not detected, skipping... "
else
	# Stop simpleAPI services
	systemctl stop simpleAPI.service
fi

%post

# Post install commands or scripts if any.
SERVICE=simpleAPI

# Start the Unit Files to control kubernetes.
echo -e "\n"
echo -e "============================="
echo -e "Starting $SERVICE services..."
echo -e "============================="
echo -e "\n"

# Reload the unit file cache
systemctl daemon-reload

# Start the Unit Files to control nucleus.
systemctl enable simpleAPI.service
systemctl start simpleAPI.service

# Start the Unit Files to control simpleAPI.
echo -e "\n"
echo -e "========================================="
echo -e " Configuration Complete:                 "
echo -e "========================================="
echo -e "\n"

systemctl status simpleAPI.service

%clean
rm -rf $RPM_BUILD_ROOT
rm -rf %{_tmppath}/%{name}
rm -rf $RPM_BUILD_ROOT/%{name}

# List files that are deployed to the server.
%files
%defattr(-,root,root)
/usr/local/bin/simpleAPI
/etc/systemd/system/simpleAPI.service


%changelog
* Wed Feb 14 2018  Richard Nason <rnason@awsdocs.com>
- First RPM Build of the simpleAPI

