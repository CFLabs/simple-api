# Simple API DevOps Lab

## Description:
-------
This lab is designed to allow a user to start from scratch and build a full DevOps pipeline using Gitlab, Jenkins, and AWS. Once complete, you will have a fully functional DevOps pipleline that builds on commit, and deploys to a new EC2 instance in AWS.

<br>

## Pre-Requisites:
-------

  * Install Git
  * Clone Lab repository
  * url: git@gitlab.com:Clusterfrak-Labs/simple-api.git 

## Infrastructure Setup:
-------

__1. &nbsp;&nbsp; Create VPC Infrastructure:__ <br>

Create a default VPC, Reference Instructuions can be found at: [http://awsdocs.com/infrastructure/create_default_vpc/](http://awsdocs.com/infrastructure/create_default_vpc/)

<br>

__2. &nbsp;&nbsp; Create Jenkins Role:__ <br>
  * Services --> IAM
  * Click on Roles
  * Click Create Role
  * Under AWS Service, Choose EC2, and then EC2 from "Select your use case". Then click Next
  * In Permissions Policies page, search for and select the following policies to be added to the role:
    ** AmazonEC2FullAccess
    ** AmazonECS_FullAccess
    ** AmazonEC2ContainerRegistryFullAccess
    ** AWSCloudFormationReadOnlyAccess
    ** AmazonS3FullAccess
    ** IAMFullAccess
  * In the review page, Name the role Jenkins, and verify that the 5 policies above have been added in the policies section.
  * Click Create Role

<br>

__3. &nbsp;&nbsp; Launch Gitlab instance via CloudFormation Template:__ <br>

  * Create Stack
  * gitlab_server_cfn.yaml from lab repo clone directory
  * Reference: http://awsdocs.com/devops/cloudformation/gitlab_cf_deployment/
  * Create User, Group, Project
  * Copy Repository URL, IE: git@ec2-1-2-3-4.compute-1.amazonaws.com:python/simple-api.git
  * CD into lab repository clone
  * rm -fr .git
  * git init
  * git remote add origin gitlab_Repository_URL
  * git add --all; git commit -m "Initial Code Commit"
  * git push origin master

<br>

__4. &nbsp;&nbsp; Launch Jenkins instance via CloudFormation Template:__ <br>
  
  * Services CloudFormation
  * Create Stack
  * simpleAPI_jenkins_server_cfn.yaml from lab repo clone directory
  * Reference: http://awsdocs.com/devops/cloudformation/jenkins_cf_deployment/
  * Run through Jenkins Setup, Use SSH command in Outputs in Cloudformation to quickly get the One Time Password Token
  * Use SSH command in Outputs in Cloudformation to quickly get the Jenkins SSH Key, Add to Gitlab as Deployment Key in Project --> Settings --> Repository --> Deployment Key
  * When Stack Complete, Go to Services -> EC2, Click on the Jenkins Instance and from the Actions menu, go to Instance Settings -> Attach/Replace IAM Role and assign Jenkins Role
  * Set up Jenkins Credentials to use SSH key in /var/lib/jenkins/.ssh
  * Install the following Plugins that will be required for the jobs we will be building
    ** Green Balls
    ** Environment Injector
    ** Version Number
    ** Copy Artifact
    ** Build-Name-Setter
    ** Checkstyle
    ** Static Analysis Collector
    ** Shelve Project
    ** Publish Over SSH
    ** Parameterized Trigger
    ** SCM Sync Configuration

<br>

__5. &nbsp;&nbsp; Create a public S3 bucket for Deployment Template:__ <br>

  * Services --> S3
  * Create bucket to hold Deployment Cloudformation Template
  * Modify the simpleAPI_deployment_base_cfn.yaml Template found in the CloudFormation folder of the lab repository and replace the SSH Key in the jenkinsConfig 02_sshkey command.
  * Upload simpleAPI_deployment_base_cfn.yaml to the bucket and copy the URL path for future use in Jenkins deployment job.
  * Ensure that the Template is public (So we don't need to set up bucket polices)

<br>

__6. &nbsp;&nbsp; Create an ECR repository to hold the Docker Image:__ <br>

  * Services --> Elastic Container Service
  * Get Started --> Cancel
  * Repositories from left menu
  * Get Started, Name Repo, Click Next Step
  * Copy Container URI, IE: 012345678910.dkr.ecr.us-east-1.amazonaws.com/simple-api:latest
  * Click the Permissions Tab
  * Name the SID Public
  * Choose Effect Allow
  * Choose Principal Everybody
  * Choose Pull only Actions from Action section
  * Save All
  * Reference: http://awsdocs.com/devops/ecr/create_repo/#setting-ecr-permissions

<br>

__7. &nbsp;&nbsp; Create Cluster and Register ECS Fargate Task Definition:__ <br>
  * Services --> EC2
  * Clusters, Create Cluster
  * Choose Networking Only option, Powered by Fargate, Next Step
  * Optional: aws ecs create-cluster --cluster-name fargate-cluster
  * Name the Cluster IE: simpleAPI, Leave Create VPC unchecked, click Create
  * Modify the simpleAPI-taskdef.json file found in the ECS folder of the lab repository and replace the image value with the proper value of your ECR Repository
  * Modify the register-task-definition.sh  file found in the ECS folder of the lab repository and replace the region value with the proper value of your region
  * Use the AWS CLI to locally register the task, or copy the files to your jenkins server that has the role in place for full access to ECS and execute register-task-definition.sh 
  * scp * ec2-user@1.2.3.4:/tmp
  * ssh ec2-user@1.2.3.4 chmod +x /tmp/register-execution-role.sh
  * ssh ec2-user@1.2.3.4 chmod +x /tmp/register-task-definition.sh
  * ssh ec2-user@1.2.3.4 cd /tmp; ./register-execution-role.sh
  * Copy the ARN of the execution Role, and update the executionRoleArn field in the simpleAPI-taskdef.json file
  * ssh ec2-user@1.2.3.4 cd /tmp; ./register-task-definition.sh
  * Modify the default Security Group to allow 80 in from anywhere (Fargate container will use the default SG)


<br>

## Jenkins Jobs:
-------
__1. &nbsp;&nbsp; simpleAPI-SST:__ <br>
  
  * Pull the code from Gitlab
  * Remove un-necessary files
  * Create a tarball archive
  * Archive the tarball to be used with future jobs
  * Kick off simpleAPI-LINT

<br>

__2. &nbsp;&nbsp; simpleAPI-LINT:__ <br>

  * Pull SST artifact and untar
  * Run a code check and scan for code standard errors
  * Kick off simpleAPI-COMPILE

<br>

__3. &nbsp;&nbsp; simpleAPI-COMPILE:__ <br>

  * Pull SST artifact and untar
  * Compile the python code into a single executable
  * Archive the Executable to be used with future jobs
  * Kick off simpleAPI-RPM

<br>

__4. &nbsp;&nbsp; simpleAPI-RPM:__ <br>
 
  * Pull SST artifact and untar
  * Pull Compiled Artifact
  * Use the included SPEC file to create an RPM using the compiled executable
  * Archive the RPM for use with future Deploy job
  * Kick off simpleAPI-Docker

<br>

__5. &nbsp;&nbsp; simpleAPI-DOCKER:__ <br>

  * Pull SST artifact and untar
  * Use the included Dockerfile to build a docker image
  * Push the built image to ECR
  * Kick off the simpleAPI-ECS-DEPLOY job

<br>

__6. &nbsp;&nbsp; simpleAPI-ECS-DEPLOY:__ <br>

  * Pull SST artifact and untar
  * Pull the RPM artifact
  * Use the included Cloudformation template to launch a deployment EC2 Instance
  * Once the instance is stood up, Deploy the RPM to the instance
  * Install the RPM on the instance, and start the service
  * Kick off the simpleAPI-ECSF-DEPLOY job

<br>

__7. &nbsp;&nbsp; simpleAPI-ECSF-DEPLOY:__ <br>

  * Deploy uploaded container image from ECR to ECS Fargate

<br>

## Jenkins Jobs Restore:
-------

> Jenkins Server:

  * cd REPO_DIR/Jenkins
  * scp jenkins.simpleAPI.demo.tar.gz ec2-user@jenkins_server_ip:/tmp
  * SSH into jenkins server
  * cd /tmp
  * tar -xzvf jenkins.simpleAPI.demo.tar.gz
  * /bin/cp -fr plugins/* /var/lib/jenkins/plugins/
  * /bin/cp -fr jobs/* /var/lib/jenkins/jobs/
  * chown -R jenkins:jenkins /var/lib/jenkins/jobs
  * chown -R jenkins:jenkins /var/lib/jenkins/plugins
  * Restart Jenkins: systemctl restart jenkins.service

<br>

> Jenkins Job Modifications:

__1. &nbsp;&nbsp; simpleAPI-SST:__ <br>

  * Configure
  * Replace GIT URL, and Set Credentials

<br>

__2. &nbsp;&nbsp; simpleAPI-Docker:__ <br>
  * Configure
  * Check/Replace values for DOCKER_REPO and REGION in Inject environment variables section

<br>

__3. &nbsp;&nbsp; simpleAPI-EC2-DEPLOY:__ <br>
  * Configure
  * Check/Replace values for S3_BUCKET, REGION, KEYNAME, VPCID, SUBNETID, SSH_LOCATION, INSTANCE_TYPE, and INSTANCE_NAME in Inject environment variables section
  
<br>

__4. &nbsp;&nbsp; simpleAPI-ECSF-DEPLOY:__ <br>
  * Configure
  * Check/Replace values for REGION, SUBNETID, CLUSTER_NAME, and TASK_DEF in Inject environment variables section

